#define BOOST_DATE_TIME_NO_LIB
#include <boost/interprocess/ipc/message_queue.hpp>
#include <iostream>
#include <vector>

#include "common.h"

using namespace boost::interprocess;
using namespace std;

int error() {
   cout << "ERROR" << endl;
   return error();
}

int main ()
{
   try{
      //Open a message queue.
      message_queue mq
         (open_only        //only create
         ,"message_queue"  //name
         );

      unsigned int priority;
      message_queue::size_type recvd_size;

      //Receive 100 numbers
      for(int i = 0; i < 100; ++i){
         int number;
         mq.receive(&number, sizeof(number), recvd_size, priority);
         cout << number << endl;
         if(number != i || recvd_size != sizeof(number))
            return error();
      }
   }
   catch(interprocess_exception &ex){
      message_queue::remove("message_queue");
      std::cout << ex.what() << std::endl;
      return error();
   }
   message_queue::remove("message_queue");

   system("pause");
   return 0;
}
