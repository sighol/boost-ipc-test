#define BOOST_DATE_TIME_NO_LIB
#include <boost/interprocess/managed_shared_memory.hpp>
#include <cstdlib> //std::system
#include <sstream>

#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>

#include "common.h"

int main (int argc, char *argv[])
{
   using namespace boost::interprocess;
   using namespace std;

   struct shm_remove
   {
      shm_remove() {  shared_memory_object::remove(MATLAB_MEMORY_NAME); }
      ~shm_remove(){  shared_memory_object::remove(MATLAB_MEMORY_NAME); }
   } remover;

   struct mutex_remove
   {
      mutex_remove() { named_mutex::remove("fstream_named_mutex"); }
      ~mutex_remove(){ named_mutex::remove("fstream_named_mutex"); }
   } mut_remover;

   //Open or create the named mutex
   named_mutex mutex(open_or_create, "fstream_named_mutex");

   managed_shared_memory segment(create_only, MATLAB_MEMORY_NAME, 1024);

   printf("free memory: %d\n", segment.get_free_memory());

   MatlabState* state = segment.construct<MatlabState>(MATLAB_MEMORY_NAME)();
   printf("state address:     %p\n", state);

   double a;

   for (int i = 0; i < 10000000; i++) {
      a += 1;
      scoped_lock<named_mutex> lock(mutex);
      double* eta = &state->x;
      for (int j = 0; j < 6; j++) {
         eta[j] = a;
      }

      MatlabState* state_ptr = segment.find<MatlabState>(MATLAB_MEMORY_NAME).first;
      // printf("state_ptr address: %p\n", state_ptr);
      // state_ptr->debug();

      // boost::this_thread::sleep(boost::posix_time::seconds(1));
   }


   return 0;
}
