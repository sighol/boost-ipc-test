#define BOOST_DATE_TIME_NO_LIB
#include <boost/interprocess/managed_shared_memory.hpp>
#include <cstdlib> //std::system
#include <sstream>

#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>

#include "common.h"

int main (int argc, char *argv[])
{
   using namespace boost::interprocess;
   using namespace std;

   managed_shared_memory segment(open_only, MATLAB_MEMORY_NAME);

   printf("free memory: %d\n", segment.get_free_memory());

   MatlabState* state_ptr = segment.find<MatlabState>(MATLAB_MEMORY_NAME).first;

   printf("state_ptr address: %p\n", state_ptr);
   state_ptr->debug();

   //Open or create the named mutex
   named_mutex mutex(open_or_create, "fstream_named_mutex");

   int dup_count = 0;
   for (int i = 0; i < 100000; i++) {
      double eta[6];
      scoped_lock<named_mutex> lock(mutex);
      memcpy(eta, &state_ptr->x, 6 * sizeof(double));
      for (int j = 1; j < 6; j++) {
         if ((&state_ptr->x)[j] != eta[j]) {
            dup_count++;
         }
      }
   }
   printf("Data races: %d\n", dup_count);



   return 0;
}
