#pragma once

#define MATLAB_MEMORY_NAME "matlabMemoryName"

struct MatlabState {
	double x, y, z, phi, theta, psi;
	double u, v, w, p, q, r;
	double dvlUpRanges[4];
	double dvlDownRanges[4];

	void debug() {
		printf("Matlab State\n");
		printf("eta: %lf, %lf, %lf, %lf, %lf, %lf\n", x, y, z, phi, theta, psi);
	}
};
