#define BOOST_DATE_TIME_NO_LIB
#include <boost/interprocess/ipc/message_queue.hpp>
#include <iostream>
#include <vector>

#include "common.h"

using namespace boost::interprocess;
using namespace std;

int error(std::string str) {
   cout << "ERROR: " << str <<  endl;
   return 1;
}

int main ()
{
   try{
      //Open a message queue.
      message_queue mq
         (open_or_create        //only create
         ,"message_queue"  //name
         ,100
         ,sizeof(int)
         );

      unsigned int priority;
      message_queue::size_type recvd_size;

      //Receive 100 numbers
      for(int i = 0; i < 100; ++i){
         int number;
         mq.receive(&number, sizeof(number), recvd_size, priority);
         cout << number << endl;
         if(number != i || recvd_size != sizeof(number))
            return error("First");
      }
   }
   catch(interprocess_exception &ex){
      message_queue::remove("message_queue");
      std::cout << ex.what() << std::endl;
      return error("Second");
   }
   message_queue::remove("message_queue");

   // system("pause");
   return 0;
}
